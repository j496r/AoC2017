
fs = require('fs')
fs.readFile('./input.txt', 'utf8', function(err, data) {
  if (err) throw err;
  solvePart1(data);
  solvePart2(data);
})


function solvePart1(data) {
  let totalSum = 0;
  data = splitDataIntoArray(data);

  data.forEach((element, index, array) => {
    if (element === array[(index + 1) % array.length])
    {
      totalSum += parseInt(element, 10);
    }
  });
  console.log("Ans part 1: " + totalSum);
}

function solvePart2(data) {
  let totalSum = 0;
  data = splitDataIntoArray(data);

  data.forEach((element, index, array) => {
    if (element === array[(index + array.length / 2) % array.length])
    {
      totalSum += parseInt(element, 10);
    }
  });
  console.log("Ans part 2: " + totalSum);
}

function splitDataIntoArray(data)
{
  return data.match(/(\d)/g);
}

