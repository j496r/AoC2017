const assert = require('assert').strict;

function solvePart1(data) {
 var stringRows = data.split(/\n/g);
 var checkSum = 0;
 stringRows.forEach((line) => {
   if (line)
   {
     var numbers = line.match(/-?\d+/g).map(Number);
     checkSum += (Math.max(...numbers) - Math.min(...numbers))
   }
 });

 return checkSum;
}

function solvePart2(data) {
 var stringRows = data.split(/\n/g);
 var checkSum = 0;
 stringRows.forEach((line) => {
   if (!line) return;

   var numbers = line.match(/-?\d+/g).map(Number);
   for (i = 0; i < numbers.length; i++)
   {
     for (j = 0; j < numbers.length; j++)
     {
       if (i !== j && numbers[i] % numbers[j] === 0)
       {
         checkSum += numbers[i] / numbers[j];
       }
     }
   }
 });

 return checkSum;
}

function runScript()
{
  fs = require('fs')
  fs.readFile('./input.txt', 'utf8', function(err, data) {
    if (err) throw err;
    console.log("Part 1 answer: " + solvePart1(data));
    console.log("Part 2 answer: " + solvePart2(data));
  });
}

// Test Part 1
assert.equal(solvePart1("5 1 9 5 \n 7 5 3 \n 2 4 6 8\n"), 18);

// Test Part 2
assert.equal(solvePart2("5 9 2 8 \n 9 4 7 3 \n 3 8 6 5\n"), 9);

runScript();


