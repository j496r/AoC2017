
console.log("Part 2 answer: " + solvePart2(265149));


function solvePart2(input) { 
  var memory = new Memory(1);
  var memorySlotCount = 1;
  var lastMemorySlot = memory.getLastMemorySlot();
  while (lastMemorySlot.entry < input)
  {
    memory.extendMemory();
    lastMemorySlot = memory.getLastMemorySlot();
    memorySlotCount++;
  }
  
  return lastMemorySlot.entry;
}
 

function MemorySlot(position, direction, data)
{
  this.position = position;
  this.direction = direction;
  this.entry = data;
}


function Memory(initialData)
{
  const initialMemoryPosition = new Matrix([0,0]);
  const initialMemoryDirection = new Matrix([1,0]);
  this.storage = [new MemorySlot(initialMemoryPosition, initialMemoryDirection, initialData)];

  var nextMemoryCorners = [[1, 0], [1, 1], [-1, 1], [-1, -1]];


  this.extendMemory = function() {
    const lastMemorySlot = this.getLastMemorySlot();

    var newPosition = matrixAdd(lastMemorySlot.position, lastMemorySlot.direction);
    var newDirection = isNextMemoryCorner(newPosition) 
       ? turnLeft(lastMemorySlot.direction)
       : lastMemorySlot.direction;

    if (isNextMemoryCorner(newPosition))
    {
      const corner = nextMemoryCorners.shift();
      nextMemoryCorners.push([corner[0] + (corner[0] > 0 ? 1 : -1),
                              corner[1] + (corner[1] > 0 ? 1 : -1)]);
    }
    var data = getMemoryNeighborhoodChecksum(newPosition, this.storage);

    this.storage.push(new MemorySlot(newPosition, newDirection, data));
  };

  this.getLastMemorySlot = function () {
    return this.storage[this.storage.length - 1];
  }

  // Helper functions
  function turnLeft(direction) {
    const leftTurnMatrix = new Matrix({rows: [[0, -1],[1, 0]]});
    return matrixMultiply(leftTurnMatrix, direction);
  }

  function isNextMemoryCorner(position)
  {
    const positionAsArray = position.getColumn(0);
    return (positionAsArray[0] === (nextMemoryCorners[0])[0] && 
            positionAsArray[1] === (nextMemoryCorners[0])[1]);
  }

  var getMemoryNeighborhoodChecksum = function (myPosition, storage)
  {
    var checksum = 0;
    const myCoordinates = myPosition.getColumn(0);
    for (var i = 0; i < storage.length; i++)
    {
      const othersCoordinates = storage[i].position.getColumn(0);
      if (Math.abs(myCoordinates[0] - othersCoordinates[0]) <= 1 &&
          Math.abs(myCoordinates[1] - othersCoordinates[1]) <= 1)
      {
        checksum += storage[i].entry;
      }
    }
    return checksum;
  }
}



// For the fun of it, lets add some basic matrix support
//
// Create new matrix:
//   new Matrix({rows: [[1,2,3],[4,5,6]]})  --> |1 2 3|
//                                              |4 5 6|
//
//   new Matrix({columns: [[1,2,3],[4,5,6]]})  --> |1 4|
//                                                 |2 5|
//                                                 |3 6|
//   new Matrix({identity: 3}) --> |1 0 0|
//                                 |0 1 0|
//                                 |0 0 1|
//
// Create vector (one column matrix):
//   new Matrix([1,2,3,4]) --> |1|
//                             |2|
//                             |3|
//                             |4|
//
// Operations:
//  Transpose matrix:
//    A.transpose()        (Note: changes the object in place)
//    matrixAdd(A, B)      (Note: creates a new matrix as answer)
//    matrixMultiply(A, B) (Note: creates a new matrix as answer)
//


function Matrix(data)
{
  this.rowMatrix = [];
  var row, col;
  if (data.rows)
  {
    this.rowMatrix = data.rows;
  }
  else if (data.columns)
  {
    for (row = 0; row < data.columns[0].length; row++)
    {
      var newRow = [];
      for (col = 0; col < data.columns.length; col++)
      {
        newRow.push(data.columns[col][row]);
      }
      this.rowMatrix.push(newRow);
    }
  }
  else if (data.identity)
  {
    var size = data.identity;
    for (row = 0; row < size; row++)
    {
      var newRow = [];
      for (col = 0; col < size; col++)
      {
        newRow.push((row === col) ? 1 : 0);
      }
      this.rowMatrix.push(newRow);
    }
  }
  else
  {
    var tempMatrix = new Matrix({columns: [data]});
    this.rowMatrix = tempMatrix.rowMatrix;
  }

  this.print = function() {
    var rowIndex;
    for (rowIndex = 0; rowIndex < this.rowMatrix.length; rowIndex++)
    {
      console.log(this.rowMatrix[rowIndex]);
    }
    console.log();
  }

  this.transpose = function (){ 
    var tempMatrix = new Matrix({columns: this.rowMatrix});
    this.rowMatrix = tempMatrix.rowMatrix;
  }

  this.getColumn = function (columnIndex)
  {
    var column = [];
    var i;
    for (i = 0; i < this.rowMatrix.length; i++)
    {
      column.push(this.rowMatrix[i][columnIndex]);
    }
    return column;
  }

  this.getRow = function (rowIndex) {
    return this.rowMatrix[rowIndex];
  }

  this.getNumRows = function () {
    return this.rowMatrix.length;
  }

  this.getNumColumns = function () {
    return this.rowMatrix[0].length;
  }
}

function matrixAdd(A, B) {
  var newRowMatrix = [];
  var m,n;
  for (m = 0; m < A.getNumRows(); m++)
  {
    var newRow = [];
    var aRow = A.getRow(m);
    var bRow = B.getRow(m);
    for (n = 0; n < A.getNumColumns(); n++)
    {
      newRow.push(aRow[n] + bRow[n]);
    }
    newRowMatrix.push(newRow);
  }
  return new Matrix({rows : newRowMatrix});
}

function matrixMultiply(A, B) { 
  var newRowMatrix = [];
  var m,n,i;
  for (m = 0; m < A.getNumRows(); m++)
  {
    var newRow = [];
    for (n = 0; n < B.getNumColumns(); n++)
    {
      var row = A.getRow(m);
      var col = B.getColumn(n);
      var newEntry = 0;
      for (i = 0; i < row.length; i++)
      {
        newEntry += row[i] * col[i];
      }
      newRow.push(newEntry);
    }
    newRowMatrix.push(newRow);
  }
  return new Matrix({rows : newRowMatrix});
}

// "2 my 4 string".match(/(\d)/g); // returns array of all matches
