import { readFileSync } from "fs";

// Test part 1
console.assert(solvePart1(1) === 0);
console.assert(solvePart1(12) === 3);
console.assert(solvePart1(23) === 2);
console.assert(solvePart1(1024) === 31);

// Solve part 1
console.log("Part 1 answer: " + solvePart1(265149));


function step(position, direction) {
  return matrixAdd(position, direction);
}

function turn(turningMatrix, direction) {
  return matrixMultiply(turningMatrix, direction);
}


function solvePart1(stopSquare) { 
  var currentPosition = new Matrix([0, 0]);
  var currentDirection = new Matrix([1, 0]);
  var leftTurnMatrix = new Matrix({rows: [[0, -1],[1, 0]]});
  var coordForNext4Corners = [[1, 0], [1, 1], [-1, 1], [-1, -1]];
  
  var currentSquare = 1;
  while (currentSquare !== stopSquare)
  {
    currentPosition = step(currentPosition, currentDirection);

    const pos = currentPosition.getColumn(0);
    if (pos[0] === (coordForNext4Corners[0])[0] && 
        pos[1] === (coordForNext4Corners[0])[1])
    {
      currentDirection = turn(leftTurnMatrix, currentDirection);
      var passedCorner = coordForNext4Corners.shift();
      coordForNext4Corners.push([passedCorner[0] + (passedCorner[0] > 0 ? 1 : -1),
                               passedCorner[1] + (passedCorner[1] > 0 ? 1 : -1)]);
    }
    currentSquare++;
  }
  
  var pos = currentPosition.getColumn(0);
  var distance = Math.abs(pos[0]) + Math.abs(pos[1]);
  console.log("Distance: " + distance);
  return distance;
}



// For the fun of it, lets add some basic matrix support
//
// Create new matrix:
//   new Matrix({rows: [[1,2,3],[4,5,6]]})  --> |1 2 3|
//                                              |4 5 6|
//
//   new Matrix({columns: [[1,2,3],[4,5,6]]})  --> |1 4|
//                                                 |2 5|
//                                                 |3 6|
//   new Matrix({identity: 3}) --> |1 0 0|
//                                 |0 1 0|
//                                 |0 0 1|
//
// Create vector (one column matrix):
//   new Matrix([1,2,3,4]) --> |1|
//                             |2|
//                             |3|
//                             |4|
//
// Operations:
//  Transpose matrix:
//    A.transpose()        (Note: changes the object in place)
//    matrixAdd(A, B)      (Note: creates a new matrix as answer)
//    matrixMultiply(A, B) (Note: creates a new matrix as answer)
//


function Matrix(data)
{
  this.rowMatrix = [];
  var row, col;
  if (data.rows)
  {
    this.rowMatrix = data.rows;
  }
  else if (data.columns)
  {
    for (row = 0; row < data.columns[0].length; row++)
    {
      var newRow = [];
      for (col = 0; col < data.columns.length; col++)
      {
        newRow.push(data.columns[col][row]);
      }
      this.rowMatrix.push(newRow);
    }
  }
  else if (data.identity)
  {
    var size = data.identity;
    for (row = 0; row < size; row++)
    {
      var newRow = [];
      for (col = 0; col < size; col++)
      {
        newRow.push((row === col) ? 1 : 0);
      }
      this.rowMatrix.push(newRow);
    }
  }
  else
  {
    var tempMatrix = new Matrix({columns: [data]});
    this.rowMatrix = tempMatrix.rowMatrix;
  }

  this.print = function() {
    var rowIndex;
    for (rowIndex = 0; rowIndex < this.rowMatrix.length; rowIndex++)
    {
      console.log(this.rowMatrix[rowIndex]);
    }
    console.log();
  }

  this.transpose = function (){ 
    var tempMatrix = new Matrix({columns: this.rowMatrix});
    this.rowMatrix = tempMatrix.rowMatrix;
  }

  this.getColumn = function (columnIndex)
  {
    var column = [];
    var i;
    for (i = 0; i < this.rowMatrix.length; i++)
    {
      column.push(this.rowMatrix[i][columnIndex]);
    }
    return column;
  }

  this.getRow = function (rowIndex) {
    return this.rowMatrix[rowIndex];
  }

  this.getNumRows = function () {
    return this.rowMatrix.length;
  }

  this.getNumColumns = function () {
    return this.rowMatrix[0].length;
  }
}

function matrixAdd(A, B) {
  var newRowMatrix = [];
  var m,n;
  for (m = 0; m < A.getNumRows(); m++)
  {
    var newRow = [];
    var aRow = A.getRow(m);
    var bRow = B.getRow(m);
    for (n = 0; n < A.getNumColumns(); n++)
    {
      newRow.push(aRow[n] + bRow[n]);
    }
    newRowMatrix.push(newRow);
  }
  return new Matrix({rows : newRowMatrix});
}

function matrixMultiply(A, B) { 
  var newRowMatrix = [];
  var m,n,i;
  for (m = 0; m < A.getNumRows(); m++)
  {
    var newRow = [];
    for (n = 0; n < B.getNumColumns(); n++)
    {
      var row = A.getRow(m);
      var col = B.getColumn(n);
      var newEntry = 0;
      for (i = 0; i < row.length; i++)
      {
        newEntry += row[i] * col[i];
      }
      newRow.push(newEntry);
    }
    newRowMatrix.push(newRow);
  }
  return new Matrix({rows : newRowMatrix});
}

// "2 my 4 string".match(/(\d)/g); // returns array of all matches
