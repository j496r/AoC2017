import { readFileSync } from "fs";
const assert = require('assert').strict;

function parseInput(filepath) {
	const input = readFileSync(filepath, { encoding: "utf-8" });
	return input.trim();
}

function solvePart1(data) {
}

//assert.equal(solvePart1("test data here"), expectedAnswerHere);


function solvePart2(data) {
}


// Usefull stuff:
// Regex: 
// "2 my 4 string".match(/(\d)/g); // returns array of all matches
//
